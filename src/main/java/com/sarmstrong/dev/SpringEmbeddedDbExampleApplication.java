package com.sarmstrong.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmbeddedDbExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEmbeddedDbExampleApplication.class, args);
	}

}
