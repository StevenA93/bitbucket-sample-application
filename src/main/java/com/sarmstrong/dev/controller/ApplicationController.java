package com.sarmstrong.dev.controller;

import com.sarmstrong.dev.model.User;
import com.sarmstrong.dev.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class ApplicationController {

    private UserService userService;

    @Autowired
    public ApplicationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public List<User> getUsers() {
        return userService.getAllUsers();
    }

}
